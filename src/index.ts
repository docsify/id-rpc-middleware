import { parseAuthHeader } from 'docsify-utils';
import * as createError from 'http-errors';
import { Context, Middleware } from 'koa';

export = class IDRPC {
  private entity: string;

  constructor(entity: string) {
    this.entity = entity;
  }

  public auth = (
    action: string,
    roles: string[] = [],
    allowForGuests = false
  ): Middleware => {
    return async (ctx: Context, next: () => Promise<any>): Promise<any> => {
      const { token } = parseAuthHeader(ctx.headers.authorization);
      const response = await ctx.rpcClient.sendCommand(
        `${this.entity}:${action}`,
        [{ token, roles }]
      );

      if (response.error) {
        throw createError(response.error.code, response.error.message);
      }

      ctx.authUser = response.result.user;
      ctx.iss = response.result.iss;

      return next();
    };
  };
};
