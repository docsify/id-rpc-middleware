import { Middleware } from 'koa';

declare class IDRPC {
  private entity: string;

  constructor(entity: string);

  public auth: IDRPC.IAuth
}

declare namespace IDRPC {
  interface IAuth {
    (action: string, roles?: string[], allowForGuests?: boolean): Middleware;
  }
}

export = IDRPC;